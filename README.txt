VIEWS CACHE EXPIRATOR API

Simple API module for easy Views cache expiration.

Author: Vojtěch Kusý <vojta@atomicant.co.uk>

Copyright: Atomic Ant Ltd. http://atomicant.co.uk

                       .---------------------------.
                  \/  <     Developed by Ants!      >
            _  _  @@  /'---------------------------'
           (_)(_)(_)
           //||||


EXAMPLE IMPLEMENTATION

/**
 * Implements hook_node_insert().
 */
function mymodule_node_insert($node) {
  mymodule_node_invalidate_views_cache($node);
}

/**
 * Implements hook_node_update().
 */
function mymodule_node_update($node) {
  mymodule_node_invalidate_views_cache($node);
}

/**
 * Implements hook_node_delete().
 */
function mymodule_node_delete($node) {
  mymodule_node_invalidate_views_cache($node);
}

/**
 * Implements hook_node_revision_delete().
 */
function mymodule_node_revision_delete($node) {
  mymodule_node_invalidate_views_cache($node);
}

/**
 * Map views invalidation to node type updates.
 *
 * @param object $node
 */
function mymodule_node_invalidate_views_cache($node) {
  switch ($node->type) {
    case 'article':
      views_cache_expirator_clear_view('homepage', 'block_1');
      break;
    case 'blog':
      views_cache_expirator_clear_view('blogs');
      views_cache_expirator_clear_view('homepage', 'block_1');
      break;
    case 'case_study':
      views_cache_expirator_clear_view('case_studies');
      break;    
    case 'event':
      views_cache_expirator_clear_view('events');
      break;
    case 'homepage_carousel':
      views_cache_expirator_clear_view('homepage_carousel');
      break;
}
